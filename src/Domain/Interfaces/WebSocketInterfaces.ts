export type TWebsocketServerEvents = 'close' | 'error' | 'connection' | 'listening' | string & {}
export type TWebsocketClientEvents = 'close' | 'error' | 'message' | 'open' | 'ping' | 'pong' | 'unexpected-response' | 'upgrade' | string & {}

export const WebsocketClientEvents: TWebsocketClientEvents[] = [
  'close',
  'error',
  'message',
  'open',
  'ping',
  'pong',
  'unexpected-response',
  'upgrade'
]

export const WebsocketServerEvents: TWebsocketServerEvents[] = [
  'close',
  'connection',
  'listening',
  'error'
]

export type TWebSocketEventCallback = (...args: any[]) => void

export interface IWebSocketHandler
{
  event: string | symbol
  callback: TWebSocketEventCallback
  once: boolean
}
