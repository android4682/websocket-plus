import { WebSocket } from "ws";

export interface IWebsocketServerClient
{
  websocket: WebSocket
  isAuthenticated: boolean
  getSkippedAuthenticatorAttempts(): number
  skippedAuthenticator(): number
  startAuthenticationTimeout(closeAfter?: number): void
  stopAuthenticationTimeout(): void
  closeConnectionByAuthenticationError(message: string): void
  getRemoteAddress(): string | null
  setRemoteAddress(remoteAddress: string): this
}