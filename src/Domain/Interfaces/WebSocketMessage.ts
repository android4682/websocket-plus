import { IStaticClass, UnknownObject } from "@android4682/typescript-toolkit";
import { RawData } from "ws";
import { WebSocketMessage } from "../../Application/Base/WebsocketMessage";
import { IWebsocketServerClient } from "./WebsocketServerClient";

export type TWebSocketMessageMode = 'broadcast' | 'direct'

export interface IDeserializableWebSocketMessage extends IStaticClass
{
  instanceOf(message: UnknownObject): boolean
  setBaseOf(message: IWebSocketMessage, instance: WebSocketMessage, sender?: IWebsocketServerClient): IWebSocketMessage
  from(message: IWebSocketMessage | string, sender?: IWebsocketServerClient): IWebSocketMessage
  fromRawData(data: RawData, sender?: IWebsocketServerClient): IWebSocketMessage
}

export type TWebsocketMessageEndpoints = 'default' | string & {}

export interface IWebSocketMessage
{
  message: string
  messageMode: TWebSocketMessageMode
  endpoint: TWebsocketMessageEndpoints
  type?: string
  originalMessage?: IWebSocketMessage
  readonly sender?: IWebsocketServerClient
  statusCode: number
}
