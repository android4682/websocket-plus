import { IStaticClass, StaticImplements } from "@android4682/typescript-toolkit";
import { RawData } from "ws";
import { WebsocketServerPlus } from "../../Application/WebsocketServerPlus";
import { AbstractClientEventHandler } from "../../Application/Base/AbstractClientEventHandler";
import { AbstractEventHandler } from "../../Application/Base/AbstractEventHandler";
import { TWebsocketClientEvents, TWebsocketServerEvents } from "./WebSocketInterfaces";
import { IWebSocketMessage } from "./WebSocketMessage";
import { IWebsocketServerClient } from "./WebsocketServerClient";

export type TIEventHandlerStatic = StaticImplements<IEventHandler, typeof AbstractEventHandler>
export interface IEventHandler extends IStaticClass
{
  eventName: string
  event: TWebsocketServerEvents
  once: boolean
  handler(server: WebsocketServerPlus, ...args: any[]): void
}

export interface IServerClientEventHandler extends IEventHandler
{
  event: TWebsocketClientEvents
  handler(server: WebsocketServerPlus, client: IWebsocketServerClient, ...args: any[]): void
}

export interface IServerClientMessageEventHandler extends IServerClientEventHandler
{
  event: 'message'
  handler(server: WebsocketServerPlus, client: IWebsocketServerClient, message: IWebSocketMessage, rawMessage: RawData, isBinary: boolean): void
}

export type TIClientEventHandlerStatic = StaticImplements<IServerClientEventHandler, typeof AbstractClientEventHandler>
export interface IClientEventHandler extends IEventHandler
{
  event: TWebsocketClientEvents
  handler(...args: any[]): void
}

export interface IClientMessageEventHandler extends IClientEventHandler
{
  event: 'message'
  handler(message: IWebSocketMessage, rawMessage: RawData, isBinary: boolean): void
}