import { IStaticClass } from "@android4682/typescript-toolkit";
import { IWebsocketServerClient } from "./WebsocketServerClient";

export const TYPE_AUTHENTICATION = 'authenticate'
export const TYPE_AUTHENTICATION_OK = 'authenticate_ok'
export const TYPE_AUTHENTICATION_FAIL = 'authenticate_fail'

export interface IWebsocketAuthenticationMessage
{
  type: typeof TYPE_AUTHENTICATION | string
  auth_method: string
}

export interface IWebsocketAuthenticator extends IStaticClass
{ 
  handler(client: IWebsocketServerClient, authMessage: IWebsocketAuthenticationMessage): Promise<boolean>
  supports(client: IWebsocketServerClient, authMessage: IWebsocketAuthenticationMessage): boolean
}
