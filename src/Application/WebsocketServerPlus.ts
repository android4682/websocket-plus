import { Delay, SpecialMap } from "@android4682/typescript-toolkit";
import { RawData, Server, ServerOptions, WebSocket } from "ws";
import { BetterConsole } from "@android4682/better-console";
import { WebSocketMessage } from "./Base/WebsocketMessage";
import { IWebsocketAuthenticator, TYPE_AUTHENTICATION_FAIL, TYPE_AUTHENTICATION_OK } from "../Domain/Interfaces/BaseWebsocketAuthenticator";
import { AuthenticationWebsocketMessage } from "./Messages/AuthenticationMessage";
import { TWebsocketClientEvents, TWebSocketEventCallback, IWebSocketHandler, TWebsocketServerEvents, WebsocketServerEvents } from "../Domain/Interfaces/WebSocketInterfaces";
import { IWebSocketMessage } from "../Domain/Interfaces/WebSocketMessage";
import { IWebsocketServerClient } from "../Domain/Interfaces/WebsocketServerClient";
import { IEventHandler, IServerClientEventHandler, IServerClientMessageEventHandler, TIClientEventHandlerStatic, TIEventHandlerStatic } from "../Domain/Interfaces/EventHandler";
import { RegisterClient_EventHandler } from "./EventHandlers/RegisterClient_EventHandler";
import { AuthTimeoutStart_EventHandler } from "./EventHandlers/AuthTimeoutStart_EventHandler";
import { Ping_ClientEventHandler } from "./EventHandlers/Ping_ClientEventHandler";
import { ClientHandling_EventHandler } from "./EventHandlers/ClientHandling_EventHandler";
import { IncomingMessage } from "http";

export type WebSocketClientEventCallback = (client: IWebsocketServerClient, ...args: any[]) => void
export interface WebSocketHandlerClient extends IWebSocketHandler
{
  event: TWebsocketClientEvents
  callback: WebSocketClientEventCallback
}

export class WebsocketServerPlus
{
  protected _ws: Server | undefined;
  protected _ws_options: ServerOptions = {}
  protected _handlers: SpecialMap<string, TIEventHandlerStatic> = new SpecialMap<string, TIEventHandlerStatic>()
  protected _client_handlers: SpecialMap<string, TIClientEventHandlerStatic> = new SpecialMap<string, TIClientEventHandlerStatic>()
  protected _authenticators: SpecialMap<string, IWebsocketAuthenticator> = new SpecialMap<string, IWebsocketAuthenticator>()
  protected _clients: IWebsocketServerClient[] = []
  protected _allowConnection: boolean = false
  protected enableDefaultAuthentication: boolean = true
  
  public constructor(options: ServerOptions)
  {
    this._ws_options = options
  }

  public get allowConnection(): boolean
  {
    return this._allowConnection
  }

  public getWebsocket(): Server
  {
    if (! this._ws) throw new Error('WebsocketServerPlus has not been initiated yet.')
    return this._ws
  }

  protected getClientFromWebsocket(ws: WebSocket): IWebsocketServerClient | undefined
  {
    return this._clients.find((client: IWebsocketServerClient) => {
      return (client.websocket === ws)
    })
  }

  public addClient(wsClient: IWebsocketServerClient): this
  {
    this._clients.push(wsClient)

    return this
  }

  public closeClient(wsClient: IWebsocketServerClient, code?: number, data?: Buffer | string): void
  {
    let indexOf = this._clients.indexOf(wsClient)
    if (indexOf !== -1) this._clients.splice(indexOf, 1)
    wsClient.websocket.close(code, data)
  }

  public async waitOnGetClientFromWebsocket(ws: WebSocket, timeout: number = 5000): Promise<IWebsocketServerClient | undefined>
  {
    let rClient: IWebsocketServerClient | undefined
    let cancel = false
    setTimeout(() => cancel = true, timeout)
    do {
      rClient = this.getClientFromWebsocket(ws)
      await Delay(100)
    } while(! rClient && ! cancel)
    return rClient
  }

  protected startWebsocketServer(): void
  {
    if (this._ws) return;
    this._ws = new Server(this._ws_options)

    for (let i = 0; i < WebsocketServerEvents.length; i++) {
      const serverEvent = WebsocketServerEvents[i];
      this._ws.on(serverEvent, (...args: any[]) => this.runListeners(serverEvent, false, ...args))
      this._ws.once(serverEvent, (...args: any[]) => this.runListeners(serverEvent, true, ...args))
    }
  }

  protected setHandlers(): void
  {
    this.addListener(RegisterClient_EventHandler, false)
    this.addListener(AuthTimeoutStart_EventHandler, false)
    this.addListener(Ping_ClientEventHandler)
    this.addListener(ClientHandling_EventHandler, false)
  }

  public initiate(): void
  {
    this.startWebsocketServer()
    this.setHandlers()
    this._allowConnection = true
  }

  public addRawListener(event: TWebsocketServerEvents, callback: TWebSocketEventCallback, once: boolean = false): this
  {
    const websocket = this.getWebsocket()

    if (once) websocket.once(event, callback)
    else websocket.addListener(event, callback)

    return this
  }

  public addListener(eventHandler: IEventHandler, client: boolean = true): this
  {
    let handlers: SpecialMap<string, TIEventHandlerStatic> = (client) ? this._client_handlers : this._handlers
    if (handlers.has(eventHandler.eventName)) {
      this.removeListener(eventHandler.eventName, client)
    }

    handlers.set(eventHandler.eventName, eventHandler)

    return this
  }

  public removeListener(name: string, client: boolean = true): this
  {
    let handlers: SpecialMap<string, TIEventHandlerStatic> = (client) ? this._client_handlers : this._handlers
    let eventHandler = handlers.get(name)
    if (! eventHandler) return this
    if (! client) this.getWebsocket().removeListener(eventHandler.event, eventHandler.handler)
    handlers.delete(name)
    return this
  }

  protected authenticateClient(client: IWebsocketServerClient, data: RawData, isBinary: boolean): void
  {
    if (client.isAuthenticated) throw new Error('Client is already authenticated, please check authentication status before calling this function.')
    let message: WebSocketMessage;
    try {
      message = WebSocketMessage.fromRawData(data, client);
    } catch (e) {
      return BetterConsole.debug('Received message in a unsupported format.', data.toString())
    }
    if (! AuthenticationWebsocketMessage.instanceOf(JSON.parse(data.toString()))) {
      // TODO: Move > amount to property, create getter and setter
      if (client.getSkippedAuthenticatorAttempts() > 3) return this.closeClient(client, 3000, 'Too many skipped authentication attempts.')
      else client.skippedAuthenticator()
      let replyMessage = new WebSocketMessage('Authentication required. Please send an authentication request first.', client, 'auth')
      replyMessage.statusCode = 401
      replyMessage.originalMessage = message
      replyMessage.type = 'authentication'
      this.send(client, replyMessage)
      return
    }
    let authMessage: AuthenticationWebsocketMessage = AuthenticationWebsocketMessage.fromRawData(data).toChildClass()
    let authenticationResponse: Promise<boolean> | null = null;
    this._authenticators.forEach((authenticator: IWebsocketAuthenticator) => {
      if (authenticator.supports(client, authMessage)) {
        authenticationResponse = authenticator.handler(client, authMessage)
        return false;
      }
    })
    if (authenticationResponse === null) {
      return client.closeConnectionByAuthenticationError('Authentication failed, probably wrong credentials or format.')
    }
    authenticationResponse = <Promise<boolean>> authenticationResponse
    authenticationResponse.then((isAuthenticated) => {
      if (isAuthenticated === false) {
        let message = new WebSocketMessage('Authentication failed', undefined, TYPE_AUTHENTICATION_FAIL)
        message.statusCode = 401
        this.send(client, message)
      } else {
        this.send(client, new WebSocketMessage('OK', undefined, TYPE_AUTHENTICATION_OK))
        client.stopAuthenticationTimeout()
      }
    })
  }

  private handleServerClientMessageEvent(eventHandler: IServerClientMessageEventHandler, client: IWebsocketServerClient, rawMessage: RawData, isBinary: boolean): void
  {
    return eventHandler.handler(this, client, WebSocketMessage.fromRawData(rawMessage, client), rawMessage, isBinary)
  }

  public runListeners(event: TWebsocketServerEvents, once: boolean = false, ...args: any[]): void
  {
    // BetterConsole.dir({ ...args })
    this._handlers.forEach((eventHandler: IEventHandler, name: string) => {
      if (eventHandler.event !== event || eventHandler.once !== once) return
      eventHandler.handler(this, ...args)
      if (once) this.removeListener(name, false)
    })
  }

  public runClientListeners(client: IWebsocketServerClient, event: TWebsocketClientEvents, once: boolean = false, ...args: any[]): void
  {
    if (event !== 'error' && event !== 'close' && client.websocket.readyState !== client.websocket.OPEN && client.websocket.readyState !== client.websocket.CONNECTING) return;
    if (event === 'message' && this.enableDefaultAuthentication && ! client.isAuthenticated && ! once) return this.authenticateClient(client, args[0], args[1])
    this._client_handlers.forEach((eventHandler: IServerClientEventHandler, name: string) => {
      if (eventHandler.event !== event || eventHandler.once !== once) return
      if (event === 'message') this.handleServerClientMessageEvent(<IServerClientMessageEventHandler> eventHandler, client, args[0], args[1])
      else eventHandler.handler(this, client, ...args)
      if (once) this.removeListener(name, true)
    })
  }

  public registerAuthenticator(key: string, handler: IWebsocketAuthenticator)
  {
    this._authenticators.set(key, handler);
  }

  public send(recipient: IWebsocketServerClient, message: IWebSocketMessage | string): Promise<boolean>
  {
    return new Promise((resolve, reject) => {
      const _message = (typeof message === 'string') ? new WebSocketMessage(message) : message
      _message.messageMode = 'direct'
      recipient.websocket.send(_message.toString(), (err) => (!err) ? resolve(true) : reject(err))
    })
  }

  public broadcast(message: IWebSocketMessage | string): Promise<boolean>
  {
    return new Promise((resolve, reject) => {
      const _message = (typeof message === 'string') ? new WebSocketMessage(message) : message
      _message.messageMode = 'broadcast'
      let promises: Promise<boolean|Error|undefined>[] = []
      for (let i = 0; i < this._clients.length; i++) promises.push(new Promise((resolve2, reject2) => this._clients[i].websocket.send(_message.toString(), (err) => (!err) ? resolve2(true) : reject2(err))))
      Promise.allSettled(promises).then(() => resolve(true)).catch((err) => reject(err))
    })
  }
}