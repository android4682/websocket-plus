import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { AbstractServerClientMessageEventHandler } from "../Base/AbstractClientMessageEventHandler";
import { WebSocketMessage } from "../Base/WebsocketMessage";
import { IWebSocketMessage } from "../../Domain/Interfaces/WebSocketMessage";
import { IWebsocketServerClient } from "../../Domain/Interfaces/WebsocketServerClient";

export class Ping_ClientEventHandler extends AbstractServerClientMessageEventHandler
{
  public static eventName: string = '__ping';

  public static handler(server: WebsocketServerPlus, client: IWebsocketServerClient, message: IWebSocketMessage, ...args: any[]): void
  {
    if (message.type !== 'ping') return
    const replyMessage = new WebSocketMessage('pong', undefined, 'ping')
    replyMessage.originalMessage = message
    server.send(client, replyMessage)
  }
}