import { IncomingMessage } from "http";
import { WebSocket } from "ws";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { AbstractEventHandler } from "../Base/AbstractEventHandler";
import { BaseWebsocketServerClient } from "../Base/BaseWebsocketServerClient";
import { TWebsocketServerEvents } from "../../Domain/Interfaces/WebSocketInterfaces";

export class RegisterClient_EventHandler extends AbstractEventHandler
{
  public static eventName: string = '__registeringClient';
  public static event: TWebsocketServerEvents = 'connection';

  public static handler(server: WebsocketServerPlus, client: WebSocket, request: IncomingMessage): void
  {
    const wsClient = new BaseWebsocketServerClient(client)
    if (! request.socket.remoteAddress) return server.closeClient(wsClient, 1007, "Couldn't determine your IP address.")
    else wsClient.setRemoteAddress(request.socket.remoteAddress)
    if (! server.allowConnection) return server.closeClient(wsClient, 1013, 'Not accepting connections yet.')
    server.addClient(wsClient)
  }
}