import { IncomingMessage } from "http";
import { WebSocket } from "ws";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { AbstractEventHandler } from "../Base/AbstractEventHandler";
import { TWebsocketServerEvents, WebsocketClientEvents } from "../../Domain/Interfaces/WebSocketInterfaces";

export class ClientHandling_EventHandler extends AbstractEventHandler
{
  public static eventName: string = '__clientHandling';
  public static event: TWebsocketServerEvents = 'connection';

  public static handler(server: WebsocketServerPlus, client: WebSocket, request: IncomingMessage): void
  {
    server.waitOnGetClientFromWebsocket(client).then((wsClient) => {
      if (! wsClient) return client.close(1011)
      for (let i = 0; i < WebsocketClientEvents.length; i++) {
        const WebsocketClientEvent = WebsocketClientEvents[i];
        wsClient.websocket.on(WebsocketClientEvent, (...args: any[]) => server.runClientListeners(wsClient, WebsocketClientEvent, false, ...args))
        wsClient.websocket.once(WebsocketClientEvent, (...args: any[]) => server.runClientListeners(wsClient, WebsocketClientEvent, true, ...args))
      }
    })
  }
}