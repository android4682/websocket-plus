import { WebSocket } from "ws";
import { IncomingMessage } from "http";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { AbstractEventHandler } from "../Base/AbstractEventHandler";
import { TWebsocketServerEvents } from "../../Domain/Interfaces/WebSocketInterfaces";

export class AuthTimeoutStart_EventHandler extends AbstractEventHandler
{
  public static eventName: string = '__authTimeoutStart';
  public static event: TWebsocketServerEvents = 'connection';

  public static handler(server: WebsocketServerPlus, client: WebSocket, request: IncomingMessage): void
  {
    server.waitOnGetClientFromWebsocket(client).then((wsClient) => {
      if (! wsClient) return client.close(1011)
      wsClient.startAuthenticationTimeout()
    })
  }
}