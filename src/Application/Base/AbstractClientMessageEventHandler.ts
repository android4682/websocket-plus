import { StaticImplements } from "@android4682/typescript-toolkit";
import { RawData } from "ws";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { IServerClientMessageEventHandler } from "../../Domain/Interfaces/EventHandler";
import { IWebSocketMessage } from "../../Domain/Interfaces/WebSocketMessage";
import { IWebsocketServerClient } from "../../Domain/Interfaces/WebsocketServerClient";
import { AbstractClientEventHandler } from "./AbstractClientEventHandler";

export class AbstractServerClientMessageEventHandler extends AbstractClientEventHandler implements StaticImplements<IServerClientMessageEventHandler, typeof AbstractServerClientMessageEventHandler>
{
  public static readonly event: "message" = "message";

  public static handler(server: WebsocketServerPlus, client: IWebsocketServerClient, message: IWebSocketMessage, rawMessage: RawData, isBinary: boolean): void
  {
    throw new Error("Method not implemented.");
  }
}