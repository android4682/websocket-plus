import { StaticImplements } from "@android4682/typescript-toolkit";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { IServerClientEventHandler } from "../../Domain/Interfaces/EventHandler";
import { TWebsocketClientEvents } from "../../Domain/Interfaces/WebSocketInterfaces";
import { IWebsocketServerClient } from "../../Domain/Interfaces/WebsocketServerClient";
import { AbstractEventHandler } from "./AbstractEventHandler";

export class AbstractClientEventHandler extends AbstractEventHandler implements StaticImplements<IServerClientEventHandler, typeof AbstractClientEventHandler>
{
  declare public static readonly event: TWebsocketClientEvents;

  public static handler(server: WebsocketServerPlus, client: IWebsocketServerClient, ...args: any[]): void
  {
    throw new Error("Method not implemented.");
  }
}