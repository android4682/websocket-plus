import { StaticImplements } from "@android4682/typescript-toolkit";
import { WebsocketServerPlus } from "../WebsocketServerPlus";
import { IEventHandler } from "../../Domain/Interfaces/EventHandler";
import { TWebsocketServerEvents } from "../../Domain/Interfaces/WebSocketInterfaces";

export class AbstractEventHandler implements StaticImplements<IEventHandler, typeof AbstractEventHandler>
{
  declare public static readonly eventName: string
  declare public static readonly event: TWebsocketServerEvents;
  public static readonly once: boolean = false;

  public static handler(server: WebsocketServerPlus, ...args: any[]): void
  {
    throw new Error("Method not implemented.");
  }
}