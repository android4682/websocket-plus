import { WebSocket } from "ws";
import { IWebsocketServerClient } from "../../Domain/Interfaces/WebsocketServerClient";

export class BaseWebsocketServerClient implements IWebsocketServerClient
{
  protected _websocket: WebSocket;
  protected _remoteAddress: string | null = null
  protected _isAuthenticated: boolean = false
  protected _skippedAuthenticator: number = 0
  protected _authenticationTimeout: NodeJS.Timeout | null = null

  public constructor(client: WebSocket)
  {
    this._websocket = client;
  }

  public get isAuthenticated(): boolean
  {
    return this._isAuthenticated
  }

  public getSkippedAuthenticatorAttempts(): number
  {
    return this._skippedAuthenticator
  }

  public skippedAuthenticator(): number
  {
    return this._skippedAuthenticator += 1
  }

  public startAuthenticationTimeout(closeAfter: number = 10000)
  {
    if (this._authenticationTimeout !== null || this._websocket.readyState !== this._websocket.OPEN) return
    this._isAuthenticated = false
    this._authenticationTimeout = setTimeout(() => this.closeConnectionByAuthenticationError(), closeAfter)
  }
  
  public stopAuthenticationTimeout()
  {
    if (this._authenticationTimeout === null || this._websocket.readyState !== this._websocket.OPEN) return
    this._isAuthenticated = true
    clearTimeout(this._authenticationTimeout)
  }

  public closeConnectionByAuthenticationError(message: string = 'Authentication failed, timeout has been reached.')
  {
    this.stopAuthenticationTimeout()
    if (this._websocket.readyState !== this._websocket.OPEN) return
    this._websocket.close(1008, message)
  }

  public get websocket(): WebSocket
  {
    return this._websocket;
  }

  public getRemoteAddress(): string | null
  {
    return this._remoteAddress;
  }

  public setRemoteAddress(remoteAddress: string)
  {
    this._remoteAddress = remoteAddress;

    return this
  }
}
