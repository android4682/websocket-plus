import { InifinityObject, Serializable, StaticClass, StaticImplements, tryConvertToJSON, UnknownObject } from "@android4682/typescript-toolkit";
import { RawData } from "ws";
import { IDeserializableWebSocketMessage, IWebSocketMessage, TWebsocketMessageEndpoints, TWebSocketMessageMode } from "../../Domain/Interfaces/WebSocketMessage";
import { IWebsocketServerClient } from "../../Domain/Interfaces/WebsocketServerClient";

export function convertRawDataToObject(data: RawData | undefined): InifinityObject
{
  if (! data) return {}
  return JSON.parse(data.toString())
}

export function convertRawDataToMessage(data: RawData | undefined, sender?: IWebsocketServerClient, expecting: TDeserializableWebSocketMessageStatic = WebSocketMessage): IWebSocketMessage
{
  if (! data) throw new Error('Data is undefined')
  if (! expecting.instanceOf(convertRawDataToObject(data))) throw new Error('Message is not an instance of given DeserializableWebSocketMessage class')
  return expecting.fromRawData(data, sender)
}

export type TDeserializableWebSocketMessageStatic = StaticImplements<IDeserializableWebSocketMessage, typeof WebSocketMessage>
export type TWebSocketMessageStatic = StaticClass<typeof WebSocketMessage>

export class WebSocketMessage implements Serializable, TDeserializableWebSocketMessageStatic, IWebSocketMessage
{
  protected _message: string
  protected _mode: TWebSocketMessageMode = 'direct'
  protected _endpoint: TWebsocketMessageEndpoints = 'default'
  protected _type?: string
  protected _replyOn?: IWebSocketMessage
  protected readonly _sender?: IWebsocketServerClient
  protected _statusCode: number = 200

  public constructor(message: string, sender?: IWebsocketServerClient, type?: TWebsocketMessageEndpoints, endpoint: string = 'default')
  {
    this._message = message
    this._endpoint = endpoint
    this._type = type
    this._sender = sender
  }

  public static instanceOf(message: UnknownObject): boolean
  {
    return ('message' in message)
  }

  public static setBaseOf(message: IWebSocketMessage, instance: WebSocketMessage, sender?: IWebsocketServerClient): WebSocketMessage
  {
    if (message.messageMode) instance._mode = message.messageMode
    if (message.type) instance._type = message.type
    if (message.originalMessage) instance._replyOn = WebSocketMessage.from(message.originalMessage, sender)
    if (message.statusCode) instance._statusCode = message.statusCode
    return instance;
  }

  public static from(message: IWebSocketMessage | string, sender?: IWebsocketServerClient): WebSocketMessage
  {
    if (typeof message === 'string') return new WebSocketMessage(message, sender)
    const wsMessage = new WebSocketMessage(message.message, sender, message.type, message.endpoint);
    return WebSocketMessage.setBaseOf(message, wsMessage, sender)
  }

  public static fromRawData(data: RawData, sender?: IWebsocketServerClient): WebSocketMessage
  {
    let resultMessage: string | IWebSocketMessage = data.toString()
    let message = tryConvertToJSON(resultMessage)
    if (typeof message === 'object') {
      if (! WebSocketMessage.instanceOf(message)) throw new Error('Given data is an object but not an instace of BaseWebSocketMessage.')
      resultMessage = <IWebSocketMessage> message
    }
    return WebSocketMessage.from(resultMessage, sender)
  }

  public get message(): string
  {
    return this._message
  }

  public set message(value: string)
  {
    this._message = value
  }

  public get messageMode(): TWebSocketMessageMode
  {
    return this._mode
  }

  public set messageMode(value: TWebSocketMessageMode)
  {
    this._mode = value
  }

  public get endpoint(): TWebsocketMessageEndpoints
  {
    return this._endpoint
  }

  public set endpoint(value: TWebsocketMessageEndpoints)
  {
    this._endpoint = value
  }

  public get type(): string | undefined
  {
    return this._type
  }

  public set type(value: string | undefined)
  {
    this._type = value
  }

  public get originalMessage(): IWebSocketMessage | undefined
  {
    return this._replyOn
  }

  public set originalMessage(value: IWebSocketMessage | undefined)
  {
    this._replyOn = value
  }
  
  public get statusCode(): number
  {
    return this._statusCode
  }
  
  public set statusCode(value: number)
  {
    this._statusCode = value
  }

  public get sender(): IWebsocketServerClient | undefined
  {
    return this._sender
  }

  public toJSON(): IWebSocketMessage
  {
    let replyMessage = undefined
    if (this._replyOn) {
      replyMessage = <WebSocketMessage> this._replyOn
    }
    return {
      message: this._message,
      messageMode: this._mode,
      endpoint: this._endpoint,
      type: this._type,
      originalMessage: replyMessage?.toJSON(),
      statusCode: this._statusCode
    }
  }

  public toString(): string
  {
    return JSON.stringify(this.toJSON())
  }
}