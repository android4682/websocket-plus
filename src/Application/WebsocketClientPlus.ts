import { SpecialMap } from "@android4682/typescript-toolkit";
import { ClientRequestArgs } from "http";
import { ClientOptions, WebSocket } from "ws"
import { TWebSocketEventCallback, IWebSocketHandler } from "../Domain/Interfaces/WebSocketInterfaces";
import { IWebSocketMessage } from "../Domain/Interfaces/WebSocketMessage";

export class WebsocketClientPlus
{
  protected _address: string | URL
  protected _protocols?: string | string[]
  protected _options?: ClientOptions | ClientRequestArgs
  protected _ws?: WebSocket
  protected _handlers: SpecialMap<string, IWebSocketHandler> = new SpecialMap<string, IWebSocketHandler>()

  public constructor(address: string | URL, protocols?: string | string[], options?: ClientOptions | ClientRequestArgs)
  {
    this._address = address
    this._protocols = protocols
    this._options = options
  }

  public initiateConnection(): WebSocket
  {
    if (this._ws) return this._ws
    console.debug('Initiating connection...', new Error().stack)
    return this._ws = new WebSocket(this._address, this._protocols, this._options)
  }

  public getWebsocket(): WebSocket
  {
    if (! this._ws) return this.initiateConnection()
    return this._ws
  }

  public on(name: string, event: string | symbol, callback: TWebSocketEventCallback): this
  {
    return this.addListener(name, event, callback, false)
  }

  public once(name: string, event: string | symbol, callback: TWebSocketEventCallback): this
  {
    return this.addListener(name, event, callback, true)
  }

  public addListener(name: string, event: string | symbol, callback: TWebSocketEventCallback, once: boolean = false): this
  {
    return this._addListener(name, event, callback, once)
  }

  private _addListener(name: string, event: string | symbol, callback: TWebSocketEventCallback, once: boolean = false): this
  {
    let handlers: SpecialMap<string, IWebSocketHandler> = this._handlers
    if (handlers.has(name)) {
      this.removeListener(name)
    }
    const WSHData: IWebSocketHandler = {
      callback,
      event,
      once
    }
    handlers.set(name, WSHData)
    return this.registerListener(WSHData)
  }

  public off(name: string): this
  {
    return this.removeListener(name)
  }

  public removeListener(name: string): this
  {
    return this._removeListener(name)
  }

  public _removeListener(name: string): this
  {
    let handlers: SpecialMap<string, IWebSocketHandler> = this._handlers
    let eventData = handlers.get(name)
    if (! eventData) return this
    handlers.delete(name)
    return this
  }

  protected registerListener(WSH: IWebSocketHandler): this
  {
    if (WSH.once) {
      this.getWebsocket().once(WSH.event, WSH.callback)
    } else {
      this.getWebsocket().on(WSH.event, WSH.callback)
    }
    return this
  }

  private runListeners(event: string | symbol, once: boolean = false, ...args: any[]): void
  {
    this._handlers.forEach((WSH: IWebSocketHandler, name: string) => {
      if (WSH.event !== event || WSH.once !== once) return
      WSH.callback(...args)
      if (once) this.removeListener(name)
    })
  }

  public send(message: IWebSocketMessage | string): Promise<boolean>
  {
    if (typeof message !== 'string') message = message.toString()
    return new Promise((resolve, reject) => this.getWebsocket().send(message, (err) => (!err) ? resolve(true) : reject(err)))
  }
}