import { InifinityObject, NonConstructor, tryConvertToJSON, SpecialMap } from "@android4682/typescript-toolkit";
import { RawData } from "ws";
import { IWebsocketAuthenticationMessage, TYPE_AUTHENTICATION } from "../../Domain/Interfaces/BaseWebsocketAuthenticator";
import { TWebSocketMessageStatic, WebSocketMessage } from "../Base/WebsocketMessage";
import { BaseWebsocketServerClient } from "../Base/BaseWebsocketServerClient";
import { IWebSocketMessage } from "../../Domain/Interfaces/WebSocketMessage";

export type AuthenticationWebsocketMessageStatic<B extends IWebsocketAuthenticationMessage> = NonConstructor<typeof AuthenticationWebsocketMessage> & { new(): B }

export interface AuthenticationWebsocketMessageObject extends IWebSocketMessage
{
  _auth_method: string
}

export class AuthenticationWebsocketMessage extends WebSocketMessage implements IWebsocketAuthenticationMessage
{
  protected _type: typeof TYPE_AUTHENTICATION = 'authenticate'
  protected _auth_method: string;

  private static children: SpecialMap<string, TWebSocketMessageStatic> = new SpecialMap<string, TWebSocketMessageStatic>()

  public constructor(message: string, auth_method: string)
  {
    super(message)
    this._auth_method = auth_method
  }

  public static registerChildClass(childClass: TWebSocketMessageStatic)
  {
    this.children.set(childClass.prototype.constructor.name, childClass)
  }

  public toChildClass(): AuthenticationWebsocketMessage
  {
    const classObj: AuthenticationWebsocketMessageObject = this.toJSON()
    let resultClass: AuthenticationWebsocketMessage = this
    AuthenticationWebsocketMessage.children.forEach((childClass: TWebSocketMessageStatic) => {
      if (childClass.instanceOf(classObj)) {
        resultClass = <AuthenticationWebsocketMessage> childClass.from(classObj, this._sender)
        return false
      }
    })
    return resultClass
  }

  public static instanceOf(message: InifinityObject): boolean
  {
    return (super.instanceOf(message) && message._type === TYPE_AUTHENTICATION && '_auth_method' in message)
  }

  public static setBaseOf(message: AuthenticationWebsocketMessageObject, instance: AuthenticationWebsocketMessage, sender?: BaseWebsocketServerClient): AuthenticationWebsocketMessage
  {
    super.setBaseOf(message, instance, sender)
    return instance
  }

  public static from(message: AuthenticationWebsocketMessageObject, sender?: BaseWebsocketServerClient): AuthenticationWebsocketMessage
  {
    let authMessage = new AuthenticationWebsocketMessage(message.message, message._auth_method)
    super.setBaseOf(message, authMessage, sender)
    authMessage._type = 'authenticate'
    return authMessage
  }

  public static fromRawData(data: RawData, sender?: BaseWebsocketServerClient): AuthenticationWebsocketMessage
  {
    let resultMessage: string | AuthenticationWebsocketMessageObject = data.toString()
    let message = tryConvertToJSON(resultMessage)
    if (typeof message !== 'object' || ! AuthenticationWebsocketMessage.instanceOf(message)) throw new Error('Given data is not an object or not an instace of AuthenticationWebsocketMessage.')
    return AuthenticationWebsocketMessage.from(<AuthenticationWebsocketMessageObject> message, sender)
  }

  public get type(): typeof TYPE_AUTHENTICATION
  {
    return this._type
  }

  public get auth_method(): string
  {
    return this._auth_method
  }

  public set auth_method(value: string)
  {
    this._auth_method = value
  }

  public toJSON(): AuthenticationWebsocketMessageObject
  {
    let baseObject = <InifinityObject> super.toJSON()
    baseObject._type = this._type
    baseObject._auth_method = this._auth_method
    return <AuthenticationWebsocketMessageObject> baseObject
  }
}