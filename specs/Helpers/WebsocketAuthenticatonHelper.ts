import { StaticImplements } from "@android4682/typescript-toolkit";
import { IWebsocketAuthenticationMessage, IWebsocketAuthenticator, BaseWebsocketServerClient, AuthenticationWebsocketMessage } from "../../dist/WebsocketServerPlus";

export const AUTH_METHODE_AUTH_MOCK = 'auth_mock'

export interface IWebsocketAuthenticatonHelperMessage extends IWebsocketAuthenticationMessage
{
}

export class WebsocketAuthenticatonHelper implements StaticImplements<IWebsocketAuthenticator, typeof WebsocketAuthenticatonHelper>
{
  public static async handler(client: BaseWebsocketServerClient, authMessage: AuthenticationWebsocketMessage): Promise<boolean>
  {
    return (authMessage.message === 'true')
  }

  public static supports(client: BaseWebsocketServerClient, authMessage: AuthenticationWebsocketMessage): boolean
  {
    return (authMessage.auth_method === AUTH_METHODE_AUTH_MOCK);
  }
}