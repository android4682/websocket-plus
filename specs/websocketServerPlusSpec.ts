import { BetterConsole } from '@android4682/better-console';
import { describe, expect, it, jest } from '@jest/globals';
import { IncomingMessage } from 'http';
import { RawData, ServerOptions, WebSocket } from "ws";
import { AuthenticationWebsocketMessage, IWebSocketMessage, BaseWebsocketServerClient, TYPE_AUTHENTICATION, TYPE_AUTHENTICATION_OK, convertRawDataToObject, convertRawDataToMessage as _convertRawDataToMessage, WebSocketMessage, TWebSocketMessageStatic } from "../dist/WebsocketServerPlus";
import { AUTH_METHODE_AUTH_MOCK, WebsocketAuthenticatonHelper } from './Helpers/WebsocketAuthenticatonHelper';
import { WebsocketClientPlusMock } from './Mocks/WebsocketClientPlusMock';
import { WebsocketServerPlusMock } from "./Mocks/WebsocketServerPlusMock";

var lastPort = 9000

function getFreePort(): number
{
  lastPort++
  return lastPort
}

function getDefaultWebsocketOption(): ServerOptions
{
  return {
    host: '127.0.0.1',
    port: getFreePort()
  }
}

function createWebsocketServerPlusForTesting(options: ServerOptions = getDefaultWebsocketOption()): WebsocketServerPlusMock {
  let server = new WebsocketServerPlusMock(options)
  server.registerAuthenticator('mockAuthenticator', WebsocketAuthenticatonHelper)
  return server
}

function createAnInitiatedWebsocketServerPlusForTesting(options: ServerOptions = getDefaultWebsocketOption()): WebsocketServerPlusMock {
  let server = createWebsocketServerPlusForTesting(options)
  server.initiate()
  return server
}

function getWebsocketURL(options: ServerOptions) {
  return `ws://${options.host}:${options.port}`
}

function convertRawDataToMessage(data: RawData | undefined, expecting: TWebSocketMessageStatic = WebSocketMessage): IWebSocketMessage
{
  return _convertRawDataToMessage(data, undefined, expecting)
}

async function authenticate(client: WebsocketClientPlusMock, server: WebsocketServerPlusMock): Promise<boolean>
{
  server.waitOnMessage()

  client.send(new AuthenticationWebsocketMessage('true', AUTH_METHODE_AUTH_MOCK))
  
  const _authMessage = convertRawDataToMessage(await client.waitOnMessage())
  console.debug({ _authMessage })
  return (_authMessage && _authMessage.type === TYPE_AUTHENTICATION_OK)
}

describe('WebsocketServerPlus', () => {
  it('should be able to be initiated', () => {
    return new Promise<boolean>((resolve, reject) => {
      const server = createWebsocketServerPlusForTesting()
      expect(server.initiate.bind(server)).not.toThrow()
      server.getWebsocket().close((err) => {
        resolve(true)
      })
    })
  })

  it('should be able to allow a client', () => {
    return new Promise<boolean>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const connectionTestHandler = (client: WebSocket, request: IncomingMessage) => {
        expect(true).toBe(true)
      }

      server.waitOnClient().then(async (serverClient) => {
        expect(serverClient).toBeInstanceOf(BaseWebsocketServerClient)
        expect(client).toBeInstanceOf(WebsocketClientPlusMock)
        if (! serverClient || ! client) return reject(new Error("Client has not yet connected to the server."))
      })
      
      const client = new WebsocketClientPlusMock(getWebsocketURL(options))
      
      client.getWebsocket().on('open', () => {
        expect(true).toBe(true)
        client.getWebsocket().close()
        server.getWebsocket().close(() => {
          resolve(true)
        })
      })
    })
  })

  it('should fail communication with the server when not authenticated', () => {
    return new Promise<void>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const client: WebsocketClientPlusMock = new WebsocketClientPlusMock(getWebsocketURL(options))

      const flow: Promise<any>[] = []

      client.getWebsocket().on('open', async () => {
        let _server_message: IWebSocketMessage | undefined
        let _client_message: IWebSocketMessage | undefined
        let receivedServerMessage: IWebSocketMessage
        let receivedClientMessage: IWebSocketMessage

        BetterConsole.debug('Test #1')
        flow.push(new Promise<void>((localResolve, localReject) => {
          server.waitOnMessage().then((data) => {
            BetterConsole.dir({ data })
            const message = convertRawDataToMessage(data)
            expect(message).toBeInstanceOf(WebSocketMessage) 
            if (! message) return localReject(new Error('First (unauthorised) message received from client is undefined.'))
            expect(message.type).toBe('TEST') 
            expect(message.message).toBe('TEST') 
            expect(message.messageMode).toBe('direct') 
            expect(message.endpoint).toBe('default') 
            expect(message.statusCode).toBe(200)
            localResolve()
          })
        }))

        const validTestMessage = new WebSocketMessage('TEST', undefined, 'TEST')
        client.send(validTestMessage.toString()).then((status) => console.debug('Message sent!')).catch((err) => reject(err))

        _client_message = convertRawDataToMessage(await client.waitOnMessage())
        expect(_client_message).toBeInstanceOf(WebSocketMessage)
        if (! _client_message) return reject(new Error("Client didn't receive a message."))

        receivedClientMessage = _client_message
        console.debug({ receivedClientMessage })
        expect(receivedClientMessage.statusCode).toBe(401)

        Promise.all(flow).then(() => {
          client.getWebsocket().close(1001)
          server.getWebsocket().close((err) => {
            expect(err).toBe(undefined)
            if (!err) resolve()
            else reject(err)
          })
        }).catch((e) => reject(e))
      })

      client.getWebsocket().on('close', (code: number, reason: Buffer) => {
        if (code === 1000 || code === 1001) return; 
        console.error({ code, reason: reason.toString() })
        reject(new Error(reason.toString()))
      })
    })
  })

  it('should fail authentication with wrong credentials', () => {
    return new Promise<void>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const client: WebsocketClientPlusMock = new WebsocketClientPlusMock(getWebsocketURL(options))

      const flow: Promise<any>[] = []

      client.getWebsocket().on('open', async () => {
        let _server_message: IWebSocketMessage | undefined
        let _client_message: IWebSocketMessage | undefined
        let receivedServerMessage: IWebSocketMessage
        let receivedClientMessage: IWebSocketMessage

        BetterConsole.debug('Test #2')
        flow.push(new Promise<void>((localResolve, localReject) => {
          server.waitOnMessage().then((data) => {
            if (! data) return localReject(new Error('No message has been received by the server.'))
            const isAuthenticationMessage = AuthenticationWebsocketMessage.instanceOf(convertRawDataToObject(data))
            expect(isAuthenticationMessage).toBe(true)
            if (! isAuthenticationMessage) return localReject(new Error('Received message from client isn\'t a authentication message.'))
            const message = <AuthenticationWebsocketMessage> convertRawDataToMessage(data, AuthenticationWebsocketMessage)
            expect(message).toBeInstanceOf(AuthenticationWebsocketMessage)
            if (! message) return localReject(new Error('Failed authorisation message received from client is undefined.'))
            expect(message.type).toBe(TYPE_AUTHENTICATION)
            expect(message.message).toBe('false') 
            expect(message.messageMode).toBe('direct') 
            expect(message.endpoint).toBe('default') 
            expect(message.statusCode).toBe(200)
            expect(message.auth_method).toBe(AUTH_METHODE_AUTH_MOCK)
            localResolve()
          })
        }))

        const failAuthMessage = new AuthenticationWebsocketMessage('false', AUTH_METHODE_AUTH_MOCK)
        client.send(failAuthMessage)

        _client_message = convertRawDataToMessage(await client.waitOnMessage())
        expect(_client_message).toBeInstanceOf(WebSocketMessage)
        if (! _client_message) return reject(new Error("Client didn't receive a message."))

        receivedClientMessage = _client_message
        console.debug({ receivedClientMessage })
        expect(receivedClientMessage.statusCode).toBe(401)

        Promise.all(flow).then(() => {
          client.getWebsocket().close(1001)
          server.getWebsocket().close((err) => {
            expect(err).toBe(undefined)
            if (!err) resolve()
            else reject(err)
          })
        }).catch((e) => reject(e))
      })

      client.getWebsocket().on('close', (code: number, reason: Buffer) => {
        if (code === 1000 || code === 1001) return; 
        console.error({ code, reason: reason.toString() })
        reject(new Error(reason.toString()))
      })
    })
  })

  it('should succesfully authenticate with correct creditentials', () => {
    return new Promise<void>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const client: WebsocketClientPlusMock = new WebsocketClientPlusMock(getWebsocketURL(options))

      const flow: Promise<any>[] = []

      client.getWebsocket().on('open', async () => {
        let _server_message: IWebSocketMessage | undefined
        let _client_message: IWebSocketMessage | undefined
        let receivedServerMessage: IWebSocketMessage
        let receivedClientMessage: IWebSocketMessage

        BetterConsole.debug('Test #3')
        flow.push(new Promise<void>((localResolve, localReject) => {
          server.waitOnMessage().then((data) => {
            if (! data) return localReject(new Error('No message has been received by the server.'))
            const isAuthenticationMessage = AuthenticationWebsocketMessage.instanceOf(convertRawDataToObject(data))
            expect(isAuthenticationMessage).toBe(true)
            if (! isAuthenticationMessage) return localReject(new Error('Received message from client isn\'t a authentication message.'))
            const message = <AuthenticationWebsocketMessage> convertRawDataToMessage(data, AuthenticationWebsocketMessage)
            expect(message).toBeInstanceOf(AuthenticationWebsocketMessage)
            if (! message) return localReject(new Error('Failed authorisation message received from client is undefined.'))
            expect(message.type).toBe(TYPE_AUTHENTICATION) 
            expect(message.message).toBe('true') 
            expect(message.messageMode).toBe('direct') 
            expect(message.endpoint).toBe('default') 
            expect(message.statusCode).toBe(200)
            expect(message.auth_method).toBe(AUTH_METHODE_AUTH_MOCK)
            localResolve()
          })
        }))

        const successAuthMessage = new AuthenticationWebsocketMessage('true', AUTH_METHODE_AUTH_MOCK)
        client.send(successAuthMessage)

        _client_message = convertRawDataToMessage(await client.waitOnMessage())
        expect(_client_message).toBeInstanceOf(WebSocketMessage)
        if (! _client_message) return reject(new Error("Client didn't receive a message."))

        receivedClientMessage = _client_message
        console.debug({ receivedClientMessage })
        expect(receivedClientMessage.statusCode).toBe(200)
        expect(receivedClientMessage.type).toBe(TYPE_AUTHENTICATION_OK)

        Promise.all(flow).then(() => {
          client.getWebsocket().close(1001)
          server.getWebsocket().close((err) => {
            expect(err).toBe(undefined)
            if (!err) resolve()
            else reject(err)
          })
        }).catch((e) => reject(e))
      })

      client.getWebsocket().on('close', (code: number, reason: Buffer) => {
        if (code === 1000 || code === 1001) return; 
        console.error({ code, reason: reason.toString() })
        reject(new Error(reason.toString()))
      })
    })
  })

  it('should succesfully authenticate with correct creditentials', () => {
    return new Promise<void>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const client: WebsocketClientPlusMock = new WebsocketClientPlusMock(getWebsocketURL(options))

      const flow: Promise<any>[] = []

      client.getWebsocket().on('open', async () => {
        let _server_message: IWebSocketMessage | undefined
        let _client_message: IWebSocketMessage | undefined
        let receivedServerMessage: IWebSocketMessage
        let receivedClientMessage: IWebSocketMessage

        BetterConsole.debug('Test #3')
        flow.push(new Promise<void>((localResolve, localReject) => {
          server.waitOnMessage().then((data) => {
            if (! data) return localReject(new Error('No message has been received by the server.'))
            const isAuthenticationMessage = AuthenticationWebsocketMessage.instanceOf(convertRawDataToObject(data))
            expect(isAuthenticationMessage).toBe(true)
            if (! isAuthenticationMessage) return localReject(new Error('Received message from client isn\'t a authentication message.'))
            const message = <AuthenticationWebsocketMessage> convertRawDataToMessage(data, AuthenticationWebsocketMessage)
            expect(message).toBeInstanceOf(AuthenticationWebsocketMessage)
            if (! message) return localReject(new Error('Failed authorisation message received from client is undefined.'))
            expect(message.type).toBe(TYPE_AUTHENTICATION) 
            expect(message.message).toBe('true') 
            expect(message.messageMode).toBe('direct') 
            expect(message.endpoint).toBe('default') 
            expect(message.statusCode).toBe(200)
            expect(message.auth_method).toBe(AUTH_METHODE_AUTH_MOCK)
            localResolve()
          })
        }))

        const successAuthMessage = new AuthenticationWebsocketMessage('true', AUTH_METHODE_AUTH_MOCK)
        client.send(successAuthMessage)

        _client_message = convertRawDataToMessage(await client.waitOnMessage())
        expect(_client_message).toBeInstanceOf(WebSocketMessage)
        if (! _client_message) return reject(new Error("Client didn't receive a message."))

        receivedClientMessage = _client_message
        console.debug({ receivedClientMessage })
        expect(receivedClientMessage.statusCode).toBe(200)
        expect(receivedClientMessage.type).toBe(TYPE_AUTHENTICATION_OK)

        Promise.all(flow).then(() => {
          client.getWebsocket().close(1001)
          server.getWebsocket().close((err) => {
            expect(err).toBe(undefined)
            if (!err) resolve()
            else reject(err)
          })
        }).catch((e) => reject(e))
      })

      client.getWebsocket().on('close', (code: number, reason: Buffer) => {
        if (code === 1000 || code === 1001) return; 
        console.error({ code, reason: reason.toString() })
        reject(new Error(reason.toString()))
      })
    })
  })

  it('should be able to communicate with the server', () => {
    return new Promise<void>((resolve, reject) => {
      const options = getDefaultWebsocketOption()
      const server = createAnInitiatedWebsocketServerPlusForTesting(options)
      
      const client: WebsocketClientPlusMock = new WebsocketClientPlusMock(getWebsocketURL(options))

      const flow: Promise<any>[] = []

      client.getWebsocket().on('open', async () => {
        let _server_message: IWebSocketMessage | undefined
        let _client_message: IWebSocketMessage | undefined
        let receivedServerMessage: IWebSocketMessage
        let receivedClientMessage: IWebSocketMessage

        expect(await authenticate(client, server)).toBe(true)

        BetterConsole.debug('Test #4')
        flow.push(new Promise<void>((localResolve, localReject) => {
          server.waitOnMessage().then((data) => {
            const message = convertRawDataToMessage(data)
            expect(message).toBeInstanceOf(WebSocketMessage)
            if (! message) return localReject(new Error('Failed authorisation message received from client is undefined.'))
            expect(message.type).toBe('ping') 
            expect(message.message).toBe('ping') 
            expect(message.messageMode).toBe('direct') 
            expect(message.endpoint).toBe('default') 
            expect(message.statusCode).toBe(200)
            localResolve()
          })
        }))

        const pingMessage = new WebSocketMessage('ping', undefined, 'ping')
        client.send(pingMessage)

        _client_message = convertRawDataToMessage(await client.waitOnMessage())
        expect(_client_message).toBeInstanceOf(WebSocketMessage)
        if (! _client_message) return reject(new Error("Client didn't receive a message."))

        receivedClientMessage = _client_message
        console.debug({ receivedClientMessage })
        expect(receivedClientMessage.statusCode).toBe(200)
        expect(receivedClientMessage.message).toBe('pong')

        Promise.all(flow).then(() => {
          client.getWebsocket().close(1001)
          server.getWebsocket().close((err) => {
            expect(err).toBe(undefined)
            if (!err) resolve()
            else reject(err)
          })
        }).catch((e) => reject(e))
      })

      client.getWebsocket().on('close', (code: number, reason: Buffer) => {
        if (code === 1000 || code === 1001) return; 
        console.error({ code, reason: reason.toString() })
        reject(new Error(reason.toString()))
      })
    })
  })
})