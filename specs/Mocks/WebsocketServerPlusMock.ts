import { IWebSocketMessage, IWebsocketServerClient, WebsocketServerPlus, WebSocketMessage } from "../../dist/WebsocketServerPlus";
import { Delay } from "@android4682/typescript-toolkit"
import { RawData, WebSocket } from "ws"
import { IncomingMessage } from "http";

type MockedEvents = "close" | "message" | "connection"

export class WebsocketServerPlusMock extends WebsocketServerPlus
{
  private _cached_messages: RawData[] = []
  
  // TODO: Need better handling
  private getFirstCachedMessage(): RawData | undefined
  {
    if (! this._cached_messages[0]) return
    return this._cached_messages.splice(0, 1)[0]
  }
  
  public async getNextClient(timeout: number = 5000): Promise<IWebsocketServerClient | undefined>
  {
    const originalClientsLength: number = this._clients.length
    // console.debug({ clients: this._clients, originalClientsLength })
    let cancel = false
    setTimeout(() => cancel = true, timeout)
    let client: IWebsocketServerClient | undefined
    do {
      client = this._clients[originalClientsLength]
      await Delay(100)
    } while (! client && ! cancel) await Delay(100)
    console.debug({ clients: this._clients, client })
    return client
  }
  
  public async waitOnMessage(timeout: number = 5000): Promise<RawData | undefined>
  {
    let cancel = false
    setTimeout(() => cancel = true, timeout)
    let message: RawData | undefined
    /* setInterval(() => {
      console.debug({ cached_messages: this._cached_messages })
    }, 500) */
    do {
      message = this.getFirstCachedMessage()
      await Delay(100)
    } while (! cancel && ! message)
    return message
  }

  public async waitOnClient(timeout: number = 5000): Promise<IWebsocketServerClient | undefined>
  {
    return this.getNextClient(timeout)
  }

  protected startWebsocketServer(): void
  {
    super.startWebsocketServer()
    
    this.addRawListener('connection', async (client: WebSocket, request: IncomingMessage) => {
      let clientPlus = this._clients.find((clientCheck: IWebsocketServerClient) => {
        return (clientCheck.websocket === client)
      })
      console.debug('Client connected')
      if (! clientPlus) {
        console.error('Client plus NOT available! Stopping Mock listener!')
        return;
      }
      console.debug('Client plus available')
      client.on('message', (data: RawData, isBinary: boolean) => {
        console.debug('Received a message: ', data.toString())
        let message: IWebSocketMessage;
        try {
          message = WebSocketMessage.fromRawData(data, clientPlus);
        } catch (e) {
          return console.error('Received message in a unsupported format.', data.toString())
        }

        console.debug('MockServer: Caching client message')
        this._cached_messages.push(data)
      })
    })
  }

  public async send(client: IWebsocketServerClient, message: string | IWebSocketMessage): Promise<boolean>
  {
    console.debug('Sending server message to client', new Error().stack)
    await Delay(500)
    return await super.send(client, message)
  }
  
  public async broadcast(message: string | IWebSocketMessage): Promise<boolean>
  {
    console.debug('Broadcasting server message', new Error().stack)
    await Delay(500)
    return await super.broadcast(message)
  }
}