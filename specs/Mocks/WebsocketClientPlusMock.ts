import { Delay } from "@android4682/typescript-toolkit";
import { ClientRequestArgs } from "http";
import { ClientOptions, RawData } from "ws";
import { WebSocketMessage, IWebSocketMessage, WebsocketClientPlus } from "../../dist/WebsocketServerPlus";

export class WebsocketClientPlusMock extends WebsocketClientPlus
{
  private _cached_messages: RawData[] = []
  
  private getFirstCachedMessage(): RawData | undefined
  {
    if (! this._cached_messages[0]) return
    return this._cached_messages.splice(0, 1)[0]
  }
  
  public async waitOnMessage(timeout: number = 5000): Promise<RawData | undefined>
  {
    let cancel = false
    setTimeout(() => cancel = true, timeout)
    let message: RawData | undefined
    do {
      message = this.getFirstCachedMessage()
      await Delay(100)
    } while (! cancel && ! message)
    return message
  }

  public constructor(address: string | URL, protocols?: string | string[], options?: ClientOptions | ClientRequestArgs)
  {
    super(address, protocols, options)

    this.getWebsocket().on('message', (data: RawData, isBinary: boolean) => {
      let message: IWebSocketMessage;
      try {
        message = WebSocketMessage.fromRawData(data);
      } catch (e) {
        return console.error('Received message in a unsupported format.', data.toString())
      }

      console.debug('MockClient: Caching server message')
      this._cached_messages.push(data)
    })
  }

  public async send(message: string | IWebSocketMessage): Promise<boolean>
  {
    console.debug('Sending client message to server', new Error().stack)
    await Delay(500)
    return await super.send(message)
  }
}