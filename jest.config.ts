import type { Config } from "@jest/types"

const config: Config.InitialOptions = {
  preset: "ts-jest",
  testEnvironment: "node",
  automock: false,
  testMatch: ["**/specs*/*+(s|S)pec.+(t|j)s"],
  globals: {
    'ts-jest': {
      tsconfig: './specs/tsconfig.json'
    }
  }
}
export default config
